from app import create_app
import asyncio

# app = asyncio.get_event_loop().run_until_complete(create_app())
app = create_app()

if __name__ == "__main__":
    import aiohttp.web
    aiohttp.web.run_app(app, port=8080)
