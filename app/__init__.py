from aiohttp import web
from app.routes import routes
from app.models import Base, engine


def create_app():
    app = web.Application()
    app.add_routes(routes)
    Base.metadata.create_all(bind=engine)

    return app
