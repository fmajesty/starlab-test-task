from aiohttp import web
from app.models import SessionLocal, Book
from datetime import datetime


routes = web.RouteTableDef()


@routes.post('/books')
async def create_book(request):
    session = SessionLocal()
    try:
        data = await request.json()
        if 'name' not in data or 'author' not in data or 'date_published' not in data:
            raise web.HTTPBadRequest(reason="Missing required fields")

        book = Book(
            name=data['name'],
            author=data['author'],
            date_published=datetime.strptime(data['date_published'], '%Y-%m-%d'),
            genre=data.get('genre')
        )
        session.add(book)
        session.commit()
        return web.json_response(status=201, data={"id": book.id, "name": book.name, "author": book.author, "date_published": book.date_published.isoformat(), "genre": book.genre})
    except Exception as e:
        session.rollback()
        raise web.HTTPBadRequest(reason=str(e))
    finally:
        session.close()


@routes.get('/books')
async def read_books(request):
    session = SessionLocal()
    try:
        query_params = request.rel_url.query
        query = session.query(Book)
        for key, value in query_params.items():
            if hasattr(Book, key):
                query = query.filter(getattr(Book, key) == value)
        books = query.all()
        return web.json_response([{"id": book.id, "name": book.name, "author": book.author, "date_published": book.date_published.isoformat(), "genre": book.genre} for book in books])
    finally:
        session.close()


@routes.get('/books/{id}')
async def get_book(request):
    session = SessionLocal()
    book_id = int(request.match_info['id'])
    try:
        book = session.query(Book).filter(Book.id == book_id).first()
        if book is None:
            raise web.HTTPNotFound(reason="Book not found")
        return web.json_response({"id": book.id, "name": book.name, "author": book.author, "date_published": book.date_published.isoformat(), "genre": book.genre})
    finally:
        session.close()
