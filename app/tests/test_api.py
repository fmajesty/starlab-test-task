import pytest
from aiohttp import web
from app import create_app


@pytest.fixture
def client(loop, aiohttp_client):
    app = create_app()
    return loop.run_until_complete(aiohttp_client(app))


@pytest.mark.asyncio
async def test_create_book(client):
    book_data = {
        "name": "Test Book",
        "author": "Johny Silverhand",
        "date_published": "2020-01-01",
        "genre": "Test Genre"
    }
    response = await client.post('/books', json=book_data)
    assert response.status == 201
    print('jopa', response)
    print(await response.json())
    assert 'id' in await response.json()


@pytest.mark.asyncio
async def test_read_books(client):
    response = await client.get('/books')
    assert response.status == 200
    books = await response.json()
    assert isinstance(books, list)


@pytest.mark.asyncio
async def test_get_book(client):
    book_data = {
        "name": "Another Test Book",
        "author": "Another Johny Silverhand",
        "date_published": "2020-02-02",
        "genre": "Another Test Genre"
    }
    post_response = await client.post('/books', json=book_data)
    book_id = (await post_response.json())['id']

    get_response = await client.get(f'/books/{book_id}')
    assert get_response.status == 200
    book = await get_response.json()
    assert book['name'] == book_data['name']
